## {ENTREGABLE_2 - BASE Y ESTRUCTURA DE DATOS}:
## CONSULTAS AVANZADAS DE LAS TABLAS DBVENTAS:

# 1.En esta consulta deseamos obtener los 10 productos más vendidos:
```sql
SELECT * FROM PRODUCTO
SELECT TOP 10 p.COD_PRO, p.DES_PRO AS NombrePRODUCTO, SUM(dv.CAN_DET ) AS TotalVendido
FROM PRODUCTO p
INNER JOIN DETALLE_COMPRA dv ON p.COD_PRO = dv.COD_PRO
GROUP BY p.COD_PRO, p.DES_PRO
ORDER BY TotalVendido DESC;
```
| COD_PRO |        NombrePRODUCTO         | TotalVendido |
|---------|-------------------------------|--------------|
| P005    | CARTUCHO TINTA NEGRA          | 1050         |
| P011    | TAJADOR METAL                 | 250          |
| P012    | TAJADOR PLASTICO              | 200          |
| P003    | PAPEL BULKY                   | 200          |
| P006    | CARTUCHO TINTA COLOR          | 200          |
| P017    | LAPICERO NEGRO                | 150          |
| P001    | PAPEL BOND A-4                | 100          |
| P013    | FOLDER MANILA OFICIO          | 100          |
| P014    | FOLDER MANILA A-4             | 100          |
| P015    | SOBRE MANILA OFICIO           | 100          |


# 2.Listamos los productos suministrados por un proveedor específico junto con las cantidades de compra.
```sql 
SELECT P.COD_PRO, P.DES_PRO, P.PRE_PRO, P.UNI_PRO, PC.CAN_DET
FROM PRODUCTO P
JOIN DETALLE_COMPRA PC ON P.COD_PRO = PC.COD_PRO
JOIN ORDEN_COMPRA OC ON PC.NUM_OCO = OC.NUM_OCO
JOIN PROVEEDOR PR ON OC.COD_PRV = PR.COD_PRV
WHERE PR.COD_PRV = 'PR10';
```
| COD_PRO | DES_PRO               | PRE_PRO | UNI_PRO | CAN_DET |
|---------|-----------------------|---------|---------|---------|
| P0015   | CARTUCHO TINTA NEGRA  | 40      | UNI     |  50     |



# 3.Listamos todas las órdenes de compra con detalles de los productos solicitados:
```sql
SELECT OC.NUM_OCO, OC.FEC_OCO,  P.RSO_PRV AS PROVEEDOR,  ODP.COD_PRO,  PR.DES_PRO AS PRODUCTO,  ODP.CAN_DET
FROM ORDEN_COMPRA OC
INNER JOIN PROVEEDOR P ON OC.COD_PRV = P.COD_PRV
INNER JOIN DETALLE_COMPRA ODP ON OC.NUM_OCO = ODP.NUM_OCO
INNER JOIN PRODUCTO PR ON ODP.COD_PRO = PR.COD_PRO
ORDER BY OC.NUM_OCO;
```
| NUM_OCO | FEC_OCO    | PROVEEDOR      | COD_PRO | PRODUCTO      | CAN_DET |
|---------|------------|----------------|---------|---------------|---------|
| OCO001  | 2013-03-05 | INVICTA        | P006    | CARTUCHO TINTA COLOR NEGRO    | 100     |
| OCO001  | 2013-03-05 | INVICTA        | P016    | SOBRE MANILA A-4    | 20     |
| OCO002  | 2013-04-08 | PETRAMAS       | P003    | PAPEL BULKY    | 200     |
| OCO002  | 2013-04-08 | PETRAMAS       | P005    | CARTUCHO TINTA NEGRA    | 500     |
| OCO003  | 2013-08-02 | MIURA          | P005    | CARTUCHO TINTA NEGRA    | 50     |
| OCO004  | 2013-04-05 | FABER CASTELL  | P009    | BORRADOR DE TINTA    | 10     |
| OCO004  | 2013-04-05 |FABER CASTELL   | P013    | FOLDER MANILA OFICIO   | 50     |
| OCO005  | 2013-03-06 | OFFICETEC      | P008    | CAJA DE DISKETTES*10    | 10     |
| OCO005  | 2013-03-06 | OFFICETEC      | P017    | LAPICERO NEGRO    | 150     |
| OCO008  | 2013-06-02 | DITO           | P002    | PAPEL BOND OFICIO    | 10     |
| OCO008  | 2013-06-02 | DITO           | P012    | TAJADOR PLASTICO    | 100     |
| OCO009  | 2013-08-03 | PRAXIS         | P009    | BORRADOR DE TINTA    | 50     |


# 4.Listamos vendedores con el total de ventas realizadas y su sueldo total.
```sql
SELECT V.COD_VEN, V.NOM_VEN + ' ' + V.APE_VEN AS VENDEDOR, SUM(DF.CAN_VEN) AS TOTAL_VENTAS, SUM(DF.CAN_VEN * DF.PRE_VEN) AS TOTAL_VENTAS_MONTO,
V.SUE_VEN AS SUELDO_MENSUAL,
COUNT(F.NUM_FAC) AS NUM_FACTURAS
FROM VENDEDOR V
LEFT JOIN FACTURA F ON V.COD_VEN = F.COD_VEN
LEFT JOIN DETALLE_FACTURA DF ON F.NUM_FAC = DF.NUM_FAC
GROUP BY V.COD_VEN, V.NOM_VEN, V.APE_VEN, V.SUE_VEN
ORDER BY TOTAL_VENTAS_MONTO DESC;
```
| COD_VEN | VENDEDOR         | TOTAL_VENTAS  | TOTAL_VENTAS_MONTO | SUELDO_MENSUAL | NUM_FACTURAS |
|---------|------------------|---------------|--------------------|----------------|--------------|
| V01     | JUANA ALVA       | NULL          | NULL               | 1000           | 0            |
| V02     | JUAN SOTO        | NULL          | NULL               | 1200           | 0            |
| V03     | CARLOS AREVALO   | NULL          | NULL               | 1500           | 0            |
| V04     | CESAR OJEDA      | NULL          | NULL               | 850            | 0            |
| V05     | JULIO VEGA       | NULL          | NULL               | 1500           | 0            |
| V06     | ANA ORTEGA       | NULL          | NULL               | 1200           | 0            |
| V07     | JOSE PALACIOS    | NULL          | NULL               | 2500           | 0            |
| V08     | RUBEN GOMEZ      | NULL          | NULL               | 1450           | 0            |
| V09     | PATRICIA ARCE    | NULL          | NULL               | 1800           | 0            |
| V10     | RENATO PEREZ     | NULL          | NULL               | 1550           | 0            |


# 5.Esta consulta nos mostrará los proveedores que han abastecido más productos junto con la cantidad total de productos abastecidos por cada uno.
```sql
SELECT PR.COD_PRV, PR.RSO_PRV,
COUNT(AB.COD_PRO) AS NUMERO_PRODUCTOS_ABASTECIDOS
FROM PROVEEDOR PR
JOIN ABASTECIMIENTO AB ON PR.COD_PRV = AB.COD_PRV
GROUP BY PR.COD_PRV, PR.RSO_PRV
ORDER BY NUMERO_PRODUCTOS_ABASTECIDOS DESC;
```

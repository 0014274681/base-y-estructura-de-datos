USE empresadb;

CREATE USER 'usuarioroot'@'localhost' IDENTIFIED BY 'Contraseñaarmy78stay.';

-- Conceder privilegios de selección en la tabla Departamento
GRANT SELECT ON EmpresaDB.Departamento TO 'usuarioroot'@'localhost';

-- Conceder privilegios de inserción en la tabla Empleado
GRANT INSERT ON EmpresaDB.Empleado TO 'usuarioroot'@'localhost';

-- Conceder privilegios de actualización en la tabla Proyecto
GRANT UPDATE ON EmpresaDB.Proyecto TO 'usuarioroot'@'localhost';


-- Revocar privilegios de selección en la tabla Departamento
REVOKE SELECT ON EmpresaDB.Departamento FROM 'usuarioroot'@'localhost';
-- Revocar privilegios de inserción en la tabla Empleado

REVOKE INSERT ON EmpresaDB.Empleado FROM 'usuarioroot'@'localhost';
-- Revocar privilegios de actualización en la tabla Proyecto

REVOKE UPDATE ON EmpresaDB.Proyecto FROM 'usuarioroot'@'localhost';


START TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('IT', 'SEATTLE');

-- Si todo va bien
COMMIT;

-- Si algo falla
ROLLBACK;


START TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('HR', 'NEW YORK');
SAVEPOINT Savepoint1;

INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento)
VALUES ('JOHN DOE', 'MANAGER', 7839, '2023-01-01', 3500, NULL, 10);

-- Rollback to Savepoint1 if needed
ROLLBACK TO Savepoint1;

-- Commit the transaction
COMMIT;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

START TRANSACTION;

INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('New Project', 'CHICAGO', 30);

COMMIT;


CREATE DATABASE EmpleadoDB;
USE EmpleadoDB;

CREATE TABLE Departamento (
    IDDepartamento INT AUTO_INCREMENT PRIMARY KEY,
    NombreDepartamento VARCHAR(50) NOT NULL UNIQUE,
    Ubicacion VARCHAR(50) DEFAULT 'UNKNOWN'
);

CREATE TABLE Empleado (
    IDEmpleado INT AUTO_INCREMENT PRIMARY KEY,
    NombreEmpleado VARCHAR(50) NOT NULL,
    Puesto VARCHAR(50),
    IDJefe INT,
    FechaContratacion DATE DEFAULT CURRENT_DATE,
    Salario DECIMAL(10,2) CHECK (Salario >= 0),
    Comision DECIMAL(10,2) CHECK (Comision >= 0),
    IDDepartamento INT,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento),
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado)
);


DELIMITER //
CREATE PROCEDURE ListarEmpleados()
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE ID INT;
    DECLARE Nombre VARCHAR(50);
    DECLARE EmpleadoCursor CURSOR FOR SELECT IDEmpleado, NombreEmpleado FROM Empleado;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    OPEN EmpleadoCursor;
    REPEAT
        FETCH EmpleadoCursor INTO ID, Nombre;
        IF NOT done THEN
            SELECT ID, Nombre;
        END IF;
    UNTIL done END REPEAT;

    CLOSE EmpleadoCursor;
END//
DELIMITER ;

CALL ListarEmpleados();

CREATE TABLE cliente (
    idcliente INT PRIMARY KEY,
    idpersona INT,
    idventa INT,
    FOREIGN KEY (idpersona) REFERENCES persona(idpersona),
    FOREIGN KEY (idventa) REFERENCES venta(idventa)
);

INSERT INTO cliente(idcliente, idpersona, idventa)
  VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);


CREATE TABLE visita_tiendas (
    idcliente INT,
    fecha_visita DATE
    );

INSERT INTO visita_tiendas(idcliente, fecha_visita)
    VALUES
(1, '2024-01-15'),
(2, '2024-01-20');
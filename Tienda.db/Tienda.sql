-- UNION, UNION ALL, INTERSECT Y EXCEPT.

Select * From venta
Select * From articulo;

select tipo_comprobante from venta where fecha >= '2024-01-01' and Fecha < '2024-02
-01'
UNION
Select nombre From articulo where  stock>0;


select p.nombre from cliente as c
inner join persona as p on c.idcliente = p.idpersona
where c.idcliente in(
    select idcliente From venta where fecha >= '2024-01-01' and fecha < '2024-02-01'
    UNION
    select idcliente From visita_tiendas where fecha_visita >= '2024-01-01' and fecha_visita <'2024-02-01'
    );


-- GROUP BY Y HAVING

-- Calcular el total de ventas por cada cliente y filtrar aquellos con total de ventas mayor a $1000
SELECT idcliente, SUM(total) AS total_ventas
FROM venta
GROUP BY idcliente
HAVING SUM(total) >= 1000;

SELECT idusuario, COUNT(*) AS ventas_completadas
FROM venta
WHERE estado = 'Pendiente'
GROUP BY idusuario
HAVING COUNT(*) >= 1;





